package com.example.luxofttest.service;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapMessage;

public class ServiceClient extends WebServiceGatewaySupport {
    public int getAddResult(int a, int b) {
        Add addRequest = new Add();
        addRequest.setIntA(a);
        addRequest.setIntB(b);
        AddResponse response =
                (AddResponse) getWebServiceTemplate().marshalSendAndReceive(addRequest, webServiceMessage -> {
                    ((SoapMessage)webServiceMessage).setSoapAction("http://tempuri.org/Add");
                });
        return response.getAddResult();
    }

    public int getDivideResult(int a, int b) {
        if (b == 0) {
            throw new IllegalArgumentException("'intB' argument can not be equals to zero");
        }
        Divide divideRequest = new Divide();
        divideRequest.setIntA(a);
        divideRequest.setIntB(b);
        DivideResponse response =
                (DivideResponse) getWebServiceTemplate().marshalSendAndReceive(divideRequest, webServiceMessage -> {
                    ((SoapMessage)webServiceMessage).setSoapAction("http://tempuri.org/Divide");
                });
        return response.getDivideResult();
    }

    public int getMultiplyResult(int a, int b) {
        Multiply m = new Multiply();
        m.setIntA(a);
        m.setIntB(b);
        MultiplyResponse response = (MultiplyResponse) getWebServiceTemplate().marshalSendAndReceive(m, webServiceMessage -> {
            ((SoapMessage)webServiceMessage).setSoapAction("http://tempuri.org/Multiply");
        });
        return response.getMultiplyResult();
    }

    public int getSubtractResult(int a, int b) {
        Subtract s = new Subtract();
        s.setIntA(a);
        s.setIntB(b);
        SubtractResponse response = (SubtractResponse) getWebServiceTemplate().marshalSendAndReceive(s, webServiceMessage -> {
            ((SoapMessage)webServiceMessage).setSoapAction("http://tempuri.org/Subtract");
        });
        return response.getSubtractResult();
    }
}
