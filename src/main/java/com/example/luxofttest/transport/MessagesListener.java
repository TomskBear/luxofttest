package com.example.luxofttest.transport;

import com.example.luxofttest.messsagesDto.QueueMesssage;
import com.google.gson.Gson;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class MessagesListener {

    Map<UUID, Integer> storage;

    public MessagesListener() {
        storage = Collections.synchronizedMap(new HashMap<>());
    }

    @JmsListener(destination = "example")
    public void receiveMessage(String text) {
        Gson gson = new Gson();
        QueueMesssage messsage = gson.fromJson(text, QueueMesssage.class);
        storage.put(messsage.getId(), messsage.getResult());
    }

    public Integer getResultById(UUID id) {
        return storage.get(id);
    }
}
