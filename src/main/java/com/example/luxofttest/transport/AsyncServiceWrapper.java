package com.example.luxofttest.transport;

import com.example.luxofttest.messsagesDto.QueueMesssage;
import com.example.luxofttest.service.ServiceClient;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class AsyncServiceWrapper {

    private final String QUEUE_NAME = "example";
    private ExecutorService executorService;

    @Autowired
    private JmsTemplate jmsSender;

    @Autowired
    private ServiceClient soapClient;

    public AsyncServiceWrapper() {
        executorService = Executors.newFixedThreadPool(10);
    }

    private void runAsync(Runnable async) {
        executorService.execute(async);
    }

    public UUID addAsync(int a, int b) {
        UUID id = UUID.randomUUID();
        runAsync(()-> {
            int result = soapClient.getAddResult(a, b);
            jmsSender.convertAndSend(QUEUE_NAME, getQueueMessage(id,result));
        });
        return id;

    }

    public UUID multiplyAsync(int a, int b) {
        UUID id = UUID.randomUUID();
        runAsync(()-> {
            int result = soapClient.getMultiplyResult(a, b);
            jmsSender.convertAndSend(QUEUE_NAME, getQueueMessage(id,result));
        });
        return id;

    }

    public UUID divideAsync(int a, int b) {
        UUID id = UUID.randomUUID();
        runAsync(()-> {
            int result = soapClient.getDivideResult(a, b);
            jmsSender.convertAndSend(QUEUE_NAME, getQueueMessage(id,result));
        });
        return id;
   }

    public UUID subtractAsync(int a, int b) {
        UUID id = UUID.randomUUID();
        runAsync(()-> {
            int result = soapClient.getSubtractResult(a, b);
            jmsSender.convertAndSend(QUEUE_NAME, getQueueMessage(id,result));
        });
        return id;
    }

    private String getQueueMessage(UUID id, int result) {
        Gson gson = new Gson();
        return gson.toJson(new QueueMesssage(id, result));
    }
}
