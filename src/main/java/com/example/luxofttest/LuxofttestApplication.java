package com.example.luxofttest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LuxofttestApplication {

    public static void main(String[] args) {
        SpringApplication.run(LuxofttestApplication.class, args);
    }

}
