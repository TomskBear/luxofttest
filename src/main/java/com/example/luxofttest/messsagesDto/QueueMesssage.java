package com.example.luxofttest.messsagesDto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class QueueMesssage {
    private UUID id;
    private int result;

    public QueueMesssage(UUID id, int result) {
        this.id = id;
        this.result = result;
    }

    public QueueMesssage() {
    }
}
