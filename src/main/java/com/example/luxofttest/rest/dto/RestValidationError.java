package com.example.luxofttest.rest.dto;


import lombok.Getter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@Getter
public class RestValidationError {
    @XmlElement(name = "fieldError")
    @XmlElementWrapper(name = "fieldErrors")
    private List<FieldErrorDto> fieldErrors = new ArrayList<>();

    public RestValidationError() {
    }

    public void addFieldError(String path, String message, String ownerClass) {
        FieldErrorDto error = new FieldErrorDto(path, message, ownerClass);
        fieldErrors.add(error);
    }
}
