package com.example.luxofttest.rest.dto;

import lombok.Getter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "fieldError")
@Getter
public class FieldErrorDto {

    @XmlElement
    private String fieldName;

    @XmlElement
    private String message;

    @XmlElement(name = "class")
    private String ownerClass;

    public FieldErrorDto() {
    }

    public FieldErrorDto(String fieldName, String messageValue, String ownerClass) {
        this.fieldName = fieldName;
        this.message = messageValue;
        this.ownerClass = ownerClass;
    }
}
