package com.example.luxofttest.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;
import java.util.UUID;

@Getter
@Setter
public class RestCorrelationId {

    @XmlElement(name="correlationId")
    @JsonProperty("correlationId")
    private UUID correlationId;

    public RestCorrelationId(UUID correlationId) {
        this.correlationId = correlationId;
    }
}
