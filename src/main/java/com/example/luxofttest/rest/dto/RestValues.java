package com.example.luxofttest.rest.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Values")
@Getter
@Setter
public class RestValues {

    @NotNull
    @NotEmpty
    private String intA;

    @NotNull
    @NotEmpty
    private String intB;
}
