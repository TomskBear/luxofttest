package com.example.luxofttest.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

@XmlRootElement(name="Result")
@Getter
@Setter
public class RestResultInfo {

    @XmlElement(name="value")
    @JsonProperty("value")
    private Integer value;

    public RestResultInfo() {

    }

    public RestResultInfo(Integer value) {
        this.value = value;
    }
}
