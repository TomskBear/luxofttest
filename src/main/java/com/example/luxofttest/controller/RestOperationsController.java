package com.example.luxofttest.controller;

import com.example.luxofttest.rest.dto.RestCorrelationId;
import com.example.luxofttest.rest.dto.RestResultInfo;
import com.example.luxofttest.rest.dto.RestValues;
import com.example.luxofttest.transport.AsyncServiceWrapper;
import com.example.luxofttest.transport.MessagesListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/rest")
public class RestOperationsController {

    @Autowired
    private AsyncServiceWrapper asyncManager;

    @Autowired
    private MessagesListener listener;

    @PostMapping("/add")
    public ResponseEntity<?> computeAddResult(@Valid @RequestBody RestValues values) {
        UUID id = asyncManager.addAsync(Integer.parseInt(values.getIntA()),Integer.parseInt(values.getIntB()));
        RestCorrelationId info = new RestCorrelationId(id);
        return new ResponseEntity<>(info, HttpStatus.OK);
    }

    @PostMapping("/divide")
    public ResponseEntity<?> computeDivideResult(@Valid @RequestBody RestValues values) {
        UUID id;
        try {
            id = asyncManager.divideAsync(Integer.parseInt(values.getIntA()), Integer.parseInt(values.getIntB()));
        }
        catch (IllegalArgumentException ex) {
            return new ResponseEntity(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }

        RestCorrelationId info = new RestCorrelationId(id);
        return new ResponseEntity(info, HttpStatus.OK);
    }

    @PostMapping("/multiply")
    public ResponseEntity<?> computeMultiplyResult(@Valid @RequestBody RestValues values) {
        UUID id = asyncManager.multiplyAsync(Integer.parseInt(values.getIntA()),Integer.parseInt(values.getIntB()));
        RestCorrelationId info = new RestCorrelationId(id);
        return new ResponseEntity(info, HttpStatus.OK);
    }

    @PostMapping("/subtract")
    public ResponseEntity<?> computeSubtractResult(@Valid @RequestBody RestValues values) {
        UUID id = asyncManager.subtractAsync(Integer.parseInt(values.getIntA()),Integer.parseInt(values.getIntB()));
        RestCorrelationId info = new RestCorrelationId(id);
        return new ResponseEntity(info, HttpStatus.OK);
    }

    @GetMapping("/result/{id}")
    public ResponseEntity<?> getAddResult(@PathVariable("id") String id) {
        Integer result = listener.getResultById(UUID.fromString(id));
        if (result == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        RestResultInfo info = new RestResultInfo(result);
        return new ResponseEntity<>(info, HttpStatus.OK);
    }

}
